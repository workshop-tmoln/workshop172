package UnitTest;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import DomainLayer.*;
import interfaces.*;
import Exceptions.*;

public class GameCenterTest {
	IGameCenter gs = new GameCenter();
	IUser usr;

	//default user
	@Before
	public void setUp() throws UserException {
		gs.register("lihi", "12345678", "12345678", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0);
		gs.register("lihi2", "12345678", "12345678", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "100/200/300", "asia", 0);
	}
	@Test
	public void NotRegisterTest()  {
		try {
			//UN
			usr = gs.register("lihi", "12345678", "12345678", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0);
			assertNull(usr);
			
			//pass
			usr = gs.register("lis", "12345678", "1678", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0);
			assertNull(usr);
		
			//name
			usr = gs.register("liq", "12345678", "12345678", "li111hi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0);
			assertNull(usr);
			
			//mail
			usr = gs.register("2li", "12345678", "12345678", "lihi", "golan", "ligiwalla.co.il", "ligiwalla.co.il", "", "1/2/3", "asia", 0);
			assertNull(usr);
			
			//date
			usr = gs.register("h3i", "12345678", "12345678", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1\2\3", "asia", 0);
			assertNull(usr);
			
			//continent
			usr = gs.register("ih4i", "12345678", "12345678", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "assssia", 0);
			assertNull(usr);
		
		} catch (UserException e) {}
		assertNull(usr);
	}
	@Test
	public void logInTest()  {
		try {
			usr = gs.login("lihi", "12345678");
		} catch (UserException e) {}
		assertNotNull(usr);	
		assertTrue(usr.isLoggedIn());
	}
	@Test
	public void NotlogInTest() throws UserException  {
		try {
			usr = gs.login("lihi", "12345678");
			usr = gs.login("lihi", "12345678");
			assertNull(usr); // - > Suppose to fail! if pass - > GOOD - in the catch.
		} catch (UserLoggedInException e) {assertNotNull(usr);}
	}
	@Test
	public void logOutTest()  {
		try {
			usr = gs.login("lihi", "12345678");
			gs.logOut("lihi");
		} catch (UserException e) {}
		assertNotNull(usr);	
		assertFalse(usr.isLoggedIn());
	}
	@Test
	public void notLogOutTest()  {
		try {
			usr = gs.login("lihi", "12345678");
			gs.logOut("lih");
		} catch (UserException e) {}
		assertTrue(usr.isLoggedIn());	
	}
	@Test
	public void editUserTest()  {
		try {
			usr = gs.login("lihi", "12345678");
			usr = gs.editUser("lihi", "11111111", "11111111", "s", "x", "", "", "", "", "", -1);
		} catch (UserException e) {}
		assertNotNull(usr);	
	}
	@Test
	public void NotEditTest()  {
		try {
			//UN
			usr = gs.editUser("ldihi", "12345678", "12345678", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0);
		} catch (UserException e) {assertNull(usr);}
			
		//pass
		try {
			usr = gs.editUser("lihi", "12345678", "1678", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0);
		} catch (UserException e) {
			assertNull(usr);
		}
			
		
		//name
		try {
			usr = gs.editUser("lihi", "12345678", "12345678", "li111hi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0);
		} catch (UserException e) {
			assertNull(usr);
		}
			
			
		//mail
		try {
			usr = gs.editUser("lihi", "12345678", "12345678", "lihi", "golan", "ligiwalla.co.il", "ligiwalla.co.il", "", "1/2/3", "asia", 0);
		} catch (UserException e) {
			assertNull(usr);
		}
		
		//date
		try {
			usr = gs.editUser("lihi", "12345678", "12345678", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1\2\3", "asia", 0);
		} catch (UserException e) {
			assertNull(usr);
		}
		
		//continent
		try {
			usr = gs.editUser("lihi", "12345678", "12345678", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "assssia", 0);
		} catch (UserException e) {
			assertNull(usr);
		}
		
		assertNull(usr);
		
		
	}
	
	
	
}
