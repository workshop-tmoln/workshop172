package UnitTest;

import static org.junit.Assert.*;
import interfaces.IGameCenter;
import interfaces.IUser;

import org.junit.Before;
import org.junit.Test;

import DomainLayer.GameCenter;
import Exceptions.LeaguesException;
import Exceptions.UserException;

public class LeagueTests {

	IGameCenter gs = new GameCenter();
	IUser usr;
	
	//default user
	@Before
	public void setUp() throws UserException {
		usr = gs.register("lihi", "12345678", "12345678", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0);
	}

	
	///////////////////////////////////////////////////////
	
			//		LEAGUE TESTS        //
	
	////////////////////////////////////////////////////////
	
	@Test
	public void SetDefaultLeagueTest(){
		try { gs.SetDefaultLeague(12, "lihi"); } catch (LeaguesException e) {}
		try { usr = gs.register("aaaa", "11111111", "11111111", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0); } catch (UserException e) {}
		assertEquals(usr.getScores(), 12);
	}
	
	@Test
	public void notSetDefaultLeagueTest() throws UserException{
		gs.register("test", "11111111", "11111111", "test", "test", "lihi@walla.com", "lihi@walla.com", "", "1/2/3", "", 0);
		try { gs.SetDefaultLeague(12, "test"); } catch (LeaguesException e) {
			usr = gs.register("test2", "11111111", "11111111", "test", "test", "lihi@walla.com", "lihi@walla.com", "", "1/2/3", "", 0);
			assertEquals(usr.getScores(), 1);
		}
	}
	
	@Test
	public void SetCriteriaMovingLeagueTest(){
		try { gs.SetCriteriaMovingLeague(50, "lihi"); } catch (LeaguesException e) {}
		try { usr = gs.register("aaaa", "11111111", "11111111", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0); } catch (UserException e) {}
		assertEquals(usr.getScores(), 1);
	}
	
	@Test
	public void notSetCriteriaMovingLeagueTest() throws UserException{
		gs.register("test", "11111111", "11111111", "test", "test", "lihi@walla.com", "lihi@walla.com", "", "1/2/3", "", 0);
		try { gs.SetCriteriaMovingLeague(50, "test"); } catch (LeaguesException e) {}
		try { usr = gs.register("aaaa", "11111111", "11111111", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0); } catch (UserException e) {}
		assertEquals(usr.getScores(), 1);
	}
	
	@Test
	public void movingUsersBetweenLeagueTest(){
		String[] s = { "aaaa"};
		try { usr = gs.register("aaaa", "11111111", "11111111", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0); } catch (UserException e) {}
		try { gs.movingUsersBetweenLeague(20,s, "lihi"); } catch (LeaguesException e) {} catch (UserException e) {}
		assertEquals(usr.getScores(), 20);
	}
	@Test
	public void notMovingUsersBetweenLeagueTest() throws UserException{
		gs.register("test", "11111111", "11111111", "test", "test", "lihi@walla.com", "lihi@walla.com", "", "1/2/3", "", 0);
		String[] s = { "aaaa"};
		try { usr = gs.register("aaaa", "11111111", "11111111", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0); } catch (UserException e) {}
		try { gs.movingUsersBetweenLeague(20,s, "test"); } catch (LeaguesException e) {} catch (UserException e) {}
		assertEquals(usr.getScores(), 1);
	}
	@Test
	public void checkForPromotedHigestTest(){
		String[] s = { "aaaa"};
		try { usr = gs.register("aaaa", "11111111", "11111111", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0); } catch (UserException e) {}
		try { gs.movingUsersBetweenLeague(20,s, "lihi"); } catch (LeaguesException e) {} catch (UserException e) {}
		gs.checkForPromotedHigest(usr);
		try {  gs.movingUsersBetweenLeague(13,s, "aaaa"); } catch (LeaguesException e) {} catch (UserException e) {}
		assertEquals(usr.getScores(), 13);
	}
	@Test
	public void notPromotedHigestTest() throws UserException{
		gs.register("test", "11111111", "11111111", "test", "test", "lihi@walla.com", "lihi@walla.com", "", "1/2/3", "", 0);
		String[] s = { "aaaa"};
		try { usr = gs.register("aaaa", "11111111", "11111111", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0); } catch (UserException e) {}
		try { gs.movingUsersBetweenLeague(20,s, "test"); } catch (LeaguesException e) {} catch (UserException e) {}
		gs.checkForPromotedHigest(usr);
		try {  gs.movingUsersBetweenLeague(13,s, "aaaa"); } catch (LeaguesException e) {} catch (UserException e) {}
		assertEquals(usr.getScores(), 1);
	}
	@Test
	public void checkForDemotedHigestTest(){
		String[] s = { "aaaa"};
		try { usr = gs.register("aaaa", "11111111", "11111111", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0); } catch (UserException e) {}
		try { gs.movingUsersBetweenLeague(800,s, "lihi"); } catch (LeaguesException e) {} catch (UserException e) {}
		assertEquals(usr.getScores(), 800);

		gs.checkForDemotedHigest("lihi");
		try {  gs.movingUsersBetweenLeague(13,s, "aaaa"); } catch (LeaguesException e) {} catch (UserException e) {}
		assertEquals(usr.getScores(), 13);
	}
	@Test
	public void notDemotedHigestTest() throws UserException{
		gs.register("test", "11111111", "11111111", "test", "test", "lihi@walla.com", "lihi@walla.com", "", "1/2/3", "", 0);
		String[] s = { "aaaa"};
		try { usr = gs.register("aaaa", "11111111", "11111111", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 0); } catch (UserException e) {}
		try { gs.movingUsersBetweenLeague(800,s, "test"); } catch (LeaguesException e) {} catch (UserException e) {}
		assertEquals(usr.getScores(), 1);

		gs.checkForDemotedHigest("lihi");
		try {  gs.movingUsersBetweenLeague(13,s, "aaaa"); } catch (LeaguesException e) {} catch (UserException e) {}
		assertEquals(usr.getScores(), 1);
	}
}
