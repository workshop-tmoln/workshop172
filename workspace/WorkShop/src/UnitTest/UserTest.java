package UnitTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import DomainLayer.GameCenter;
import DomainLayer.User;
import Exceptions.UserException;
import Exceptions.UserLoggedInException;
import Exceptions.UserNameNotExistException;
import Exceptions.WrongPasswordException;

public class UserTest {
	GameCenter gs = new GameCenter();
	
	//default user
	@Before
	public void setUp() throws UserException {
		gs.register("lihi", "12345678", "12345678", "lihi", "golan", "ligi@walla.co.il", "ligi@walla.co.il", "", "1/2/3", "asia", 10);
	}
	
	@Test
	public void promotedTest() throws WrongPasswordException, UserLoggedInException, UserNameNotExistException {
		User usr = gs.login("lihi", "12345678");
		usr.promoteUser(200);
		assertTrue(usr.getScores() == 201);
	}

	@Test
	public void demotedTest() throws WrongPasswordException, UserLoggedInException, UserNameNotExistException {
		User usr = gs.login("lihi", "12345678");
		usr.demoteUser(200);
		assertTrue(usr.getScores() == 0);
	}
	
	@Test
	public void notPromotedTestNotLogin() {
		User usr = new User("a", "1", "a", "a", "a", null, null, "asia", 100, gs, 10);
		usr.promoteUser(-1);
		assertTrue(usr.getScores() == 10);
	}
	@Test
	public void notPromotedTestNotEnoughPoint() throws WrongPasswordException, UserLoggedInException, UserNameNotExistException {
		User usr = gs.login("lihi", "12345678");
		usr.promoteUser(-1);
		assertTrue(usr.getScores() == 1);
	}
	@Test
	public void demotedTestFailLogIn() throws WrongPasswordException, UserLoggedInException, UserNameNotExistException {
		User usr = new User("a", "1", "a", "a", "a", null, null, "asia", 100, gs, 10);
		usr.demoteUser(-1);
		assertTrue(usr.getScores() == 10);
	}
	@Test
	public void demotedTestFailpoints() throws WrongPasswordException, UserLoggedInException, UserNameNotExistException {
		User usr = gs.login("lihi", "12345678");
		usr.demoteUser(-1);
		assertTrue(usr.getScores() == 1);
	}
}
