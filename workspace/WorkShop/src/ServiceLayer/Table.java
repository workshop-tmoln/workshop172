package ServiceLayer;

import java.util.Map;

public abstract class Table {


	public abstract boolean start();
	public abstract int calcStatistics();

}
