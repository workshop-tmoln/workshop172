package ServiceLayer;

public abstract class Game {
	private int gameID;
	private State _active;
	
	public abstract boolean start();
	public abstract boolean fold (User user);
	public abstract boolean check (User user);
	public abstract boolean raise (User user, double sum);
	public abstract boolean call (User user,double sum);
	public abstract int calcStatistics();

}
