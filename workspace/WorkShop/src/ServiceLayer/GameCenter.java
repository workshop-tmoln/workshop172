package ServiceLayer;

import java.awt.image.BufferedImage;
import java.util.Date;

public abstract class GameCenter {
	private int[] LeagueDistribution;
	private User[] users;
	private Table[] activeTables;

	
	public abstract boolean joinGame(User user,Game game);

	public abstract User login(String username, String password);
	public abstract User register(String userName, String password,String pass, String fName,String lName,
			String email, String avatar,Date birthday,String continent);
	public abstract User editUser(String userName, String password, String fName,
			String lName,String email, String avatar,Date birthday,String continent);
	public abstract boolean updateLog(String log);
	public abstract boolean saveFinishGame(Game game);
	public abstract void addNotifications(Notification not);

}
