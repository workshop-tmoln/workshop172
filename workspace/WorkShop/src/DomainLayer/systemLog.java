package DomainLayer;

import java.awt.List;
import java.util.ArrayList;

import interfaces.IsystemLog;

public class systemLog implements IsystemLog{

	  private ArrayList<String> systemLogs = new ArrayList<String>();
	  private ArrayList<String> errorLogs = new ArrayList<String>();

	  private final static systemLog INSTANCE = new systemLog();
	 
	  // Private constructor suppresses generation of a (public) default constructor
	  private systemLog() {}
	 
	  public static systemLog getInstance() {
	    return INSTANCE;
	  }
	@Override
	public void systemLog(String log) {systemLogs.add(log);}

	@Override
	public void errorLog(String log) {errorLogs.add(log);}
}