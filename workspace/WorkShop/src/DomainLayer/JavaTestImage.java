package DomainLayer;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.security.auth.login.LoginException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import Exceptions.UserException;
 

//WORK!!

public class JavaTestImage {
   
    static JFrameWin jFrameWindow;
 
  
    
	public static void main(String[] args) throws LoginException, UserException, ParseException  {
		//HashMap<Integer, Integer> h = new HashMap<Integer, Integer>();
	//	h.put(1, 2);
	//	System.out.println(h.get(2));
	/*	try{
			throw new UserException();
		}catch(UserException u){
			System.out.println(u);
		}
		*/
    	//SwingUtilities.invokeLater(runJFrameLater);
		Date today = new Date();
		today.setHours(0); today.setMinutes(0); today.setSeconds(0);
       
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy"); 
        //Date birDay = null;
        Date aft = df.parse("05/20/2017");
        
        System.out.println(aft.compareTo(today));
       
        long diff = aft.getTime() - today.getTime();
        System.out.println ("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        
	}
    public static class JFrameWin extends JFrame{
          
        public JFrameWin(){
            this.setTitle("java-buddy.blogspot.com");
          //Our choosen size:
            this.setSize(300, 200);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
            BufferedImage bufferedImage = null;
            try {
                bufferedImage = ImageIO.read(this.getClass().getResource("java.jpg"));
            } catch (IOException ex) {
                Logger.getLogger(JavaTestImage.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            
             BufferedImage resizeImg = resize(bufferedImage,500,200);
            JLabel jLabel = new JLabel(new ImageIcon(resizeImg));
            
          //Image size:
            this.setSize(resizeImg.getWidth(),resizeImg.getHeight());
             
            JPanel jPanel = new JPanel();
            jPanel.add(jLabel);
            this.add(jPanel);
           // System.out.println(this.getHeight()+" "+this.getWidth());

        }
        
        
        
        
        private BufferedImage resize(BufferedImage img, int newW, int newH) { 
            Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
            BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

            Graphics2D g2d = dimg.createGraphics();
            g2d.drawImage(tmp, 0, 0, null);
            g2d.dispose();

            return dimg;
        }  
          
    }
   
     
    static Runnable runJFrameLater = new Runnable() {
          
        @Override
        public void run() {
            jFrameWindow = new JFrameWin();
            jFrameWindow.setVisible(true);
        }
       
    };
    
  
 
}