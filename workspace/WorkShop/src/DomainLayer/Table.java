package DomainLayer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import ServiceLayer.User;
import interfaces.ICommunication;
import interfaces.IGame;
import interfaces.IGameCenter;
import interfaces.ITable;
import interfaces.IUser;



public class Table implements ITable{
	private int tableID;
	private Map<IUser, Double> participants;
	private List<IUser> spectating;
	private IUser creator;
	private ICommunication chat;
	private IGameCenter gameCenter;
	private IGame currentGame;
	
	public Table() {}
	
	public void init(int id, IUser creator, double creatorMoney){
		this.tableID=id;
		participants=new HashMap<>();
		spectating=new Vector<>();
		this.creator=creator;
		chat=new Communication();
		participants.put(creator, creatorMoney);
		
	}
	
	@Override
	public boolean start() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int calcStatistics() {
		// TODO Auto-generated method stub
		return 0;
	}

}
