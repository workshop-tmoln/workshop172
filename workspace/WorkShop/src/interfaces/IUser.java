package interfaces;

public interface IUser {
//	private String userName;
//	private String hashedPassword;
	
	
	public abstract void notifyall();
	public abstract String getUserName();
	public abstract int getScores();
	public void demoteUser(int score);
	public void promoteUser(int score);
	public void logIn();
	public boolean isLoggedIn();
	


}
