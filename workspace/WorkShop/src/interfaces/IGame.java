package interfaces;

public interface IGame {
//	private int gameID;
//	private State _active;
	
	public abstract boolean start();
	public abstract boolean fold (IUser user);
	public abstract boolean check (IUser user);
	public abstract boolean raise (IUser user, double sum);
	public abstract boolean call (IUser user,double sum);
	public abstract int calcStatistics();

}
