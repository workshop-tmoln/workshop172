package interfaces;

import java.util.Map;

public interface ITableStub {


	public abstract boolean start();
	public abstract int calcStatistics();

}
