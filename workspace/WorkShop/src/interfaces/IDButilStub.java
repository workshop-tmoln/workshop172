package interfaces;

import java.awt.image.BufferedImage;
import java.util.Date;

public interface IDButilStub {
	
	public abstract IUser login(String username, String password);
	public abstract IUser register(String userName, String password, String fName,String lName,String email1,String email2, String avatar,String birthday,String continent,double wallet);
	public abstract IUser editUser(String userName,String password,String fName,String lName,String email1,String email2,
			String avatar,String birthday,String continent,double wallet);
	public abstract boolean updateLog(String log);
	public abstract boolean saveFinishGame(IGame game);
	public abstract void addNotifications(INotificationStub not);

}
