package interfaces;

import Exceptions.LeaguesException;
import Exceptions.UserException;

public interface IGameCenter {

	
	public abstract boolean joinGame(IUser user,IGame game);

	public abstract IUser login(String username, String password) throws UserException;
	public void logOut(String username) throws UserException;
	public abstract IUser register(String userName, String password, String pass, String fName,
			String lName, String email1, String email2, String avatar,
			String birthday, String continent, double wallet) throws UserException;
	public abstract IUser editUser(String userName,String password,String pass,String fName,String lName,String email1,
			String email2, String avatar,String birthday,String continent,double wallet) throws UserException;
	public abstract boolean updateLog(String log) throws UserException;
	public abstract boolean saveFinishGame(IGame game) throws UserException;
	public abstract void addNotifications(INotificationStub not) throws UserException;
	
	
	public abstract void writeLog(String classs, String func, String err,String name) throws UserException;

	public abstract void checkForPromotedHigest(IUser user) ;
	public abstract void checkForDemotedHigest(String user) ;
	public void SetDefaultLeague(int leg, String callerName) throws LeaguesException;
	public void SetCriteriaMovingLeague(int scores, String callerName) throws LeaguesException;
	public void movingUsersBetweenLeague(int leg, String[] moveUsers, String callerName)  throws LeaguesException, UserException;

	public abstract int getPreviousLeague(int myScore);


}
